﻿using System;
using PrestaCap.Reporting.Services.Abstraction;
using PrestaCap.Reporting.Services.Implementation.DataSources;
using PrestaCap.Reporting.Services.Implementation.ExcelProcessor;
using PrestaCap.Reporting.Services.Implementation.Output;

namespace PrestaCap.Reporting.Hosts.ConsoleSamples
{
    class Program
    {
        /// <summary>
        /// Works on:
        /// * Config file from S3
        /// * Source file from S3
        /// * Output file on S3
        /// </summary>
        static void Sample_1()
        {
            Init();
            var bucket = Environment.GetEnvironmentVariable("BUCKET");
            var sourceFileKey = "source/hotelrates.json";
            var configFileKey = Environment.GetEnvironmentVariable("CONFIG_KEY");
            IExcelProcessor processor = new InitWorkSheets(new S3DataSource(bucket, configFileKey),
                new S3DataSource(bucket, sourceFileKey),
                new S3Output());
            InitSteps(processor);
            processor.Execute();
        }

        /// <summary>
        /// Works on:
        /// * Config file from Local Storage
        /// * Source file from  S3
        /// * Output file on S3
        /// </summary>
        static void Sample_2()
        {
            Init();
            var bucket = Environment.GetEnvironmentVariable("BUCKET");
            var sourceFileKey = "source/hotelrates.json";
            IExcelProcessor processor = new InitWorkSheets(new FileSystemDataSource("Config.json"),
                new S3DataSource(bucket, sourceFileKey),
                new S3Output());
            InitSteps(processor);
            processor.Execute();
        }


        /// <summary>
        /// Works on:
        /// * Config file from S3
        /// * Source file from Local Storage
        /// * Output file on S3
        /// </summary>
        static void Sample_3()
        {
            Init();
            var bucket = Environment.GetEnvironmentVariable("BUCKET");
            var configFileKey = Environment.GetEnvironmentVariable("CONFIG_KEY");
            IExcelProcessor processor = new InitWorkSheets(new S3DataSource(bucket, configFileKey),
                new FileSystemDataSource("hotelrates.json"),
                new S3Output());
            InitSteps(processor);
            processor.Execute();
        }


        /// <summary>
        /// Works on:
        /// * Config file from S3
        /// * Source file from S3
        /// * Output file on FileSystem
        /// </summary>
        static void Sample_4()
        {
            Init();
            var bucket = Environment.GetEnvironmentVariable("BUCKET");
            var sourceFileKey = "source/hotelrates.json";
            var configFileKey = Environment.GetEnvironmentVariable("CONFIG_KEY");
            IExcelProcessor processor = new InitWorkSheets(new S3DataSource(bucket, configFileKey),
                new S3DataSource(bucket, sourceFileKey),
                new FileSystemOutput());
            InitSteps(processor);
            processor.Execute();
        }

        static void Init()
        {
            Environment.SetEnvironmentVariable("BUCKET", "prestacap-reporting");
            Environment.SetEnvironmentVariable("OUTPUT_KEY_PREFIX", "reports/");
            Environment.SetEnvironmentVariable("CONFIG_KEY", "config/Config.json");
            Environment.SetEnvironmentVariable("LOCAL_OUTPUT_PATH", "E:\\");

           

        }   

        static void InitSteps(IExcelProcessor processor)
        {
            processor.SetNextStep(new ProcessHeaders().SetNextStep(new CellsMapper().SetNextStep(new SaveWorkbook())));
        }

        static void Main(string[] args)
        {
            const string template =
                "{0}. Read Config file ({1}) ,  Source file from ({2}) and Outputfile will be placed on ({3})";
            Console.WriteLine("Please Select one of the below Scenarios or type other number to exit:");
            Console.WriteLine(template, 1, "S3", "S3", "S3");
            Console.WriteLine(template, 2, "Local Storage", "S3", "S3");
            Console.WriteLine(template, 3, "S3", "Local Storage", "S3");
            Console.WriteLine(template, 4, "S3", "S3", "Local Storage");


            var choice = 1;
            while (choice  > 0 && choice < 5)
            {
                 choice =  int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Sample_1();
                        break;
                    case 2:
                        Sample_2();
                        break;

                    case 3:
                        Sample_3();
                        break;

                    case 4:
                        Sample_4();
                        break;


                }

                Console.WriteLine("Done, Check the output");
            }

        }
    }
}