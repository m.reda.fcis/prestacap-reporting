using System;
using Amazon.Lambda.Core;
using Amazon.Lambda.S3Events;
using PrestaCap.Reporting.Services.Abstraction;
using PrestaCap.Reporting.Services.Implementation.DataSources;
using PrestaCap.Reporting.Services.Implementation.ExcelProcessor;
using PrestaCap.Reporting.Services.Implementation.Output;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace PrestaCap.Reporting.Hosts.Lambda
{
    public class Function
    {
        private IExcelProcessor _processor;


        /// <summary>
        /// This method is called for every Lambda invocation. This method takes in an S3 event object and can be used 
        /// to respond to S3 notifications.
        /// </summary>
        /// <param name="evnt"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public void FunctionHandler(S3Event evnt, ILambdaContext context)
        {
            try
            {
                Execute(evnt);
            }
            catch (Exception e)
            {
                context.Logger.LogLine(e.Message);
                context.Logger.LogLine(e.InnerException?.Message);
                context.Logger.LogLine(e.StackTrace);
            }
        }

        private void Execute(S3Event evnt)
        {
            InitProcessor(evnt);
            _processor.Execute();
        }

        private void InitProcessor(S3Event evnt)
        {
            var fileInfo = GetReportFileInfo(evnt);
            var configFileInfo = ConfigFileInfo;

            _processor = new InitWorkSheets(new S3DataSource(configFileInfo.Bucket, configFileInfo.Key),
                new S3DataSource(fileInfo.Bucket, fileInfo.Key), new S3Output());

            _processor.SetNextStep(new ProcessHeaders().SetNextStep(new CellsMapper().SetNextStep(new SaveWorkbook())));
        }

        private static (string Bucket, string Key) GetReportFileInfo(S3Event evnt)
        {
            return (evnt.Records[0]?.S3.Bucket.Name, evnt.Records[0]?.S3.Object.Key);
        }


        private static (string Bucket, string Key) ConfigFileInfo => (Environment.GetEnvironmentVariable("BUCKET"),
            Environment.GetEnvironmentVariable("CONFIG_KEY"));
    }
}
