


# Prestacap Reporting Tool:

[![Download Npm](https://img.shields.io/badge/npm-%3E=%20v6.9.4-blue.svg)](https://nodejs.org/en/download/) [![Download dot-net core](https://img.shields.io/badge/dotnet--core-%3E=%202.1.4-blue.svg)](https://www.microsoft.com/net/download/dotnet-core/sdk-2.0.0)  [![Download VS 2017](https://img.shields.io/badge/visual--studio-2017-blue.svg)](https://www.visualstudio.com/thank-you-downloading-visual-studio/?sku=Community&rel=15) [![Download AWS-Tools](https://img.shields.io/badge/aws--tools-%3E=%201.14.4.1-blue.svg)](https://marketplace.visualstudio.com/items?itemName=AmazonWebServices.AWSToolkitforVisualStudio2017)[
![Download Entity framework Core](https://img.shields.io/badge/aws--cli-%3E=%201.16-blue.svg)](https://s3.amazonaws.com/aws-cli/AWSCLI64PY3.msi)


# What is Presta-Cap Reporting Tool
Prestacap reporting tool is a cloud based tool that fetch data from JSON file to Excel file.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See **deployment** for notes on how to deploy the project on a live system.
### Installing

 1. Make sure that all the required software are installed.
 2. restore nuget packages using visual studio or by executing below script in the solution root folder
 ```
 dotnet restore
 ```


### Deployment

Deployment for this project has became easy - Thanks to NPM Scripts.
Follow the below steps:

1. Install amazon tools globally by executing the below command
`dotnet tool install -g Amazon.Lambda.Tools`	

2. Using file explorer , integrated power-shell or command line and head to `PrestaCap.Reporting.Hosts.Lambda` folder.
3. Execute `npm run deploy` this command will deploy the code to   **AWS Lambda Function**


### How to use:
To know how to use this tool, please refer to the sample console application in the solution.
It contains 4 of the scenarios could be done using this tool.

### Unit Tests:
Thera are set of unit testing provided in the solution, running unit testing is limited to be one class at the time due to some static objects creation.

Running multiple classes at the same time might yield to failure.

## Prerequisites :
Config file must be existed either as System file or on S3 **config** folder to be able to use this tool.
Please refer to the Samples provided for more instructions

## High Level of Reporting Tool E2E:

```mermaid
sequenceDiagram
User ->> source S3 Folder: New *.json file
source S3 Folder ->> Lambda Function: Notification for the *.json file Uploaded
Note right of source S3 Folder: The notification will have the bucket name and the key of the uploaded json file.
activate Lambda Function
Lambda Function ->> config folder: read the configuration file.
Lambda Function ->> output folder: writing Excel extracted.
```



## End to End Cycle Details:
1. When *.json file is uploaded on **prestacap-reporting** bucket - **source** folder, S3 Event is sent to **prestacap-reporting** Lambda Function.
2. The Lambda function will parse the S3 Event and then it will get the file information [ bucket-name and key].
3. The function will read the source json file and the config file from S3 bucket.
4.  The data extraction will be processed based on the configuration file.
5. After the extraction is done, the Excel file will be placed on **prestacap-reporting** bucket - **output** folder.
6. The Excel file will be named following the below mask:
	**dd.MMM-hh:mm** 
	*dd: day of the month: (e.g: 10)*
	*MMM: month (e.g: Dec)*
	*hh: hour of the day (e.g: 18)*
	*mm: minutes*
	*file name example : 08.Oct -13:50 , this indicates that this file has been created on 8th of October on 13:50 UTC*


## Features:
1. Source JSON file could be read from S3 or File System.
2. Config file could be read from S3 or File System.
3. Output could be written on S3 or File System.
4. Reporting tool could parse any JSON file to an Excel based on the configuration file - *refer to Config file structure section*




## Config File Structure:
### `headers`:
headers holds the information about which columns will be selected and the selection criteria as below:

* `name`: the name of the column that will be displayed in Excel file.
* `jsonPath` JSON path of the node that the values of column will be selected from. This is following **LINQ to JSON**  syntax.
* `operation` This define what is the operation will be performed on the values of the excel.  
	* `name` defines the operation name, currently we have only 4 operation which are:
		* `DirectMapping` indicates that the values will be selected from the JSON file without any post processing.
		* `DateManipulation` manipulate the date value by adding a specific number of days.
		* `BooleanBitConversion` Converting the Boolean value to the corresponding bit. Which means that `true` will be converted to 1 and `false` will be converted to 1

	* `params`	 the parameters the operation might need.


* `type` data type of the values that will be selected.
* `formatting` Excel format of the column.
* `cached` Boolean value that indicates whether to cache the values after being selected to be used in upcoming operations or not.


### `properties`:
Hold the properties that will be applied globally on the Excel file:

* `columnWidth` column width 
* `autoFilter` indicates whether to apply auto filtering on the columns or not.
* `sheetName` Excel sheet name.
* `alignment` Horizontal allignment of the text, it has 3 values as 
	*	`Center`
	*	`CenterContinuous`
	*	`Right`
	*	`Fill`
	*	`Distributed`
	*	`Justify`
* `wrapText` indicate whether to wrap text or not


## AWS Component References
Those are the AWS Component used in this project. You could access them using the console access provided.

1. **AWS Lambda Function:** *prestacap-report-genrator*
2. **AWS S3**:
	*	**Bucket Name**: *prestacap-reporting
	*	**Configuration Folder**: config/
	*	**Input Folder**: source/
	*	**Output Folder**: output/

## Automating Sending Mail
We could automate this process by apply *step function* approach if the file less than **6 MB**  -- *[refer to AWS Lambda Limits](https://docs.aws.amazon.com/lambda/latest/dg/limits.html)*
1. Rather than placing the output file on S3, the **Lambda Function** could return the stream output.
2. The Stream output will be passed to another **Lambda Function** that could be responsible for sending the mails using **SES**

If the file is more than **6 MB**
3. When the output Excel file is placed on S3 Bucket, a S3 Event will invoke another Lambda function
4. The Second Lambda Function will be responsible for attaching the file in the mail and send the mails using **SES**



## Need Help ? 
If any help is needed, please don't hesitate to drop me a mail on mohamed.reda.shousha@gmail.com 

