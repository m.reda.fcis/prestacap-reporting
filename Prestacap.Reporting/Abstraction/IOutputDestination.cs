﻿using System.IO;

namespace PrestaCap.Reporting.Services.Abstraction
{
    public interface IOutputDestination
    {
        /// <summary>
        /// Save the output from memory stream
        /// </summary>
        /// <param name="stream">memory stream that stores the information that needs to be saved</param>
        void Save(MemoryStream stream);
    }
}
