﻿namespace PrestaCap.Reporting.Services.Abstraction
{
    public interface IExcelProcessor
    {
        void Execute();

        IExcelProcessor SetNextStep(IExcelProcessor nextStep);


    }
}
