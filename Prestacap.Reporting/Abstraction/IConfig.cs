﻿namespace PrestaCap.Reporting.Services.Abstraction
{
    public interface IDataSource
    {
        /// <summary>
        ///     Get data from data soruce and deserialize to type T
        ///      </summary>
        /// <typeparam name="T">Type that the file will be deserialized to</typeparam>
        /// <returns>Typed object that represents the JSON file</returns>
        T GetData<T>();
    }
}
