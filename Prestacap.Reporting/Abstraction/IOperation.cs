﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using PrestaCap.DataContracts.ExcelParserConfig;

namespace PrestaCap.Reporting.Services.Abstraction
{
    public interface IOperation
    {
        IEnumerable<object> Operate(JToken sourceJson, Header header);
    }
}