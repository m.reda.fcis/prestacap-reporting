﻿using System;
using System.Linq;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using PrestaCap.DataContracts.ExcelParserConfig;
using PrestaCap.Reporting.Services.Abstraction;

namespace PrestaCap.Reporting.Services.Implementation.ExcelProcessor
{
    public abstract class BaseExcelProcessor : IExcelProcessor, IDisposable
    {
        #region Protected Memebers

        protected IExcelProcessor NextStep;
        protected static ExcelPackage Package;
        protected static ParserConfig ParserConfig;
        protected static IOutputDestination OutputDestination;
        protected static JToken SourceJson;

        /// <summary>
        /// Get the Main workbook sheet we are working on - Needs enhancements in case we are working on more than sheet
        /// </summary>
        protected static ExcelWorksheet MainWorkSheet => Package.Workbook.Worksheets.FirstOrDefault();

        #endregion

        #region Constructors

        protected BaseExcelProcessor()
        {
        }

        protected BaseExcelProcessor(IDataSource configSource, IDataSource fileSource,
            IOutputDestination outputDestination)
        {
            Init(configSource, fileSource, outputDestination);
        }

        #endregion

        private static void Init(IDataSource configSource, IDataSource fileSource, IOutputDestination outputDestination)
        {
            OutputDestination = outputDestination;
            ParserConfig = configSource.GetData<ParserConfig>();
            SourceJson = fileSource.GetData<JToken>();
            Package = new ExcelPackage();
        }

        /// <summary>
        /// Set the next step that will be executed in the chain
        /// </summary>
        /// <param name="nextStep"></param>
        public IExcelProcessor SetNextStep(IExcelProcessor nextStep)
        {
            NextStep = nextStep;
            return this;
        }

        #region Public Methods

        public abstract void Execute();

        public void Dispose()
        {
            //Package.Dispose();
        }

        #endregion
    }
}
