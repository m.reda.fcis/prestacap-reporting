﻿using System.Collections.Generic;
using System.Linq;
using PrestaCap.DataContracts.ExcelParserConfig;
using PrestaCap.Reporting.Services.Implementation.Operations;

namespace PrestaCap.Reporting.Services.Implementation.ExcelProcessor
{
    public class CellsMapper : BaseExcelProcessor
    {
        public override void Execute()
        {
            for (var index = 0; index < ParserConfig.Headers.Count; index++)
            {
                var header = ParserConfig.Headers[index];

                var headerValues = OperationContext.Operate(SourceJson, header).ToList();


                AppendCells(headerValues, index);

                FormatCells(index, headerValues.Count, header);
            }

            MainWorkSheet.Cells[MainWorkSheet.Dimension.Address].AutoFilter = ParserConfig.Properties.AutoFilter;

            NextStep.Execute();
        }

        private static void AppendCells(IReadOnlyList<object> values, int index)
        {
            for (var valueIndex = 0; valueIndex < values.Count(); valueIndex++)
            {
                // as you can see, we start from row 2 becasue the first row is preserved for column headers
                MainWorkSheet.Cells[(valueIndex + 2), (index + 1)].Value = values[valueIndex];
            }
        }

        private static void FormatCells(int columnIndex, int rowCount, Header header)
        {
            /*Start from row 2 because the first row is preserved for the headers.
             *Start from column 1 because the excel is not zero indexed. The first column starts from 1 */

            MainWorkSheet.Cells[2, (columnIndex + 1), (rowCount + 2), (columnIndex + 1)].Style.Numberformat.Format =
                header.Formatting;
        }
    }
}
