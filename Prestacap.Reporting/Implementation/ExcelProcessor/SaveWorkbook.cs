﻿using System.IO;

namespace PrestaCap.Reporting.Services.Implementation.ExcelProcessor
{
    public class SaveWorkbook : BaseExcelProcessor
    {
        public override void Execute()
        {
            var outputStream = new MemoryStream();
            Package.SaveAs(outputStream);
            OutputDestination.Save(outputStream);
        }
    }
}
