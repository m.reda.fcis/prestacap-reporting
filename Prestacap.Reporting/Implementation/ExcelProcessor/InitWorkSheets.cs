﻿using OfficeOpenXml.Style;
using PrestaCap.Misc.Utilities;
using PrestaCap.Reporting.Services.Abstraction;

namespace PrestaCap.Reporting.Services.Implementation.ExcelProcessor
{
    public class InitWorkSheets : BaseExcelProcessor
    {

        private static string MainSheetName => ParserConfig.Properties.SheetName;

        public InitWorkSheets(IDataSource configSource, IDataSource fileSource, IOutputDestination outputDestination) :
            base(configSource, fileSource, outputDestination)
        {
        }


        public override void Execute()
        {
            Init();
            NextStep.Execute();
        }

        #region Private Methods

        /// <summary>
        /// Initialize Working workbook
        /// </summary>
        private static void Init()
        {
            InitBaseWorkSheet();
            InitFormatting();
        }

        /// <summary>
        /// Intialize the base workbook sheet
        /// </summary>
        private static void InitBaseWorkSheet()
        {
            Package.Workbook.Worksheets.Add(MainSheetName);
        }

        /// <summary>
        /// Format the workbook sheet intially
        /// </summary>
        private static void InitFormatting()
        {

            MainWorkSheet.DefaultColWidth = ParserConfig.Properties.ColumnWidth;
            MainWorkSheet.Cells.Style.HorizontalAlignment =
                EnumUtility.TryParse<ExcelHorizontalAlignment>(ParserConfig.Properties.Alignment);
            MainWorkSheet.Cells.Style.WrapText = ParserConfig.Properties.WrapText;
           
        }
        #endregion
        
        
    }
}
