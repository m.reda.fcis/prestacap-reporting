﻿using System.Collections.Generic;
using System.Linq;

namespace PrestaCap.Reporting.Services.Implementation.ExcelProcessor
{
    public class ProcessHeaders:BaseExcelProcessor
    {
      

        public override void Execute()
        {
            // Populates the column headers
            MainWorkSheet.Cells.LoadFromArrays(new List<string[]>(){ ParserConfig.Headers.Select(c => c.Name).ToArray() });
            NextStep.Execute();
        }

    }
}
