﻿using System;
using System.IO;
using PrestaCap.Misc.Enums;
using PrestaCap.Misc.Extensions;
using PrestaCap.Reporting.Services.Abstraction;

namespace PrestaCap.Reporting.Services.Implementation.Output
{
    public class FileSystemOutput:IOutputDestination
    {
      
        public void Save(MemoryStream stream)
        {
            File.WriteAllBytes(Path.Combine(Environment.GetEnvironmentVariable("LOCAL_OUTPUT_PATH"),$"{DateTime.UtcNow:dd.MMM-hh.mm}").AppendFileExtension(FileExt.ExcelXlsx),stream.ToArray());

        }
    }
}
