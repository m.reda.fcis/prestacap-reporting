﻿using System;
using System.IO;
using Amazon;
using PrestaCap.Misc.Enums;
using PrestaCap.Misc.Extensions;
using PrestaCap.Reporting.Services.Abstraction;
using PrestaCap.S3.Repository;

namespace PrestaCap.Reporting.Services.Implementation.Output
{
    public class S3Output : IOutputDestination
    {
        private static readonly IS3Repoistory S3Repoistory;

        static S3Output()
        {

#if DEBUG
            S3Repoistory = new S3Repository("AKIAIE4XYJQS6BZLB3JQ", "xwe3qmu/rjbKMGawkX60dwVwn+kEhsNRDUC4m2SN", RegionEndpoint.USEast1);
#else
            S3Repoistory = new S3Repository();
#endif
           ;
        }

        public void Save(MemoryStream stream)
        {
           var x = S3Repoistory.UploadObject(OutputBucket,
                OutputKey, stream, ContentType.Excel).Result;
        }

        private string OutputBucket => Environment.GetEnvironmentVariable("BUCKET");

        private string OutputKey =>
            $"{Environment.GetEnvironmentVariable("OUTPUT_KEY_PREFIX")}{DateTime.UtcNow:dd.MMM-hh.mm}"
                .AppendFileExtension(FileExt.ExcelXlsx);
    }
}