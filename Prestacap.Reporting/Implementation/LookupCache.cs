﻿using System.Collections.Generic;
using Microsoft.Extensions.Caching.Memory;

namespace PrestaCap.Reporting.Services.Implementation
{
    public static class LookupCache
    {
        private static readonly IMemoryCache Cache;

         static LookupCache()
        {
            Cache = new MemoryCache(new MemoryCacheOptions());
        }

        /// <summary>
        /// Set Enumarbale values of the cache
        /// </summary>
        /// <param name="key">Key of the cached collection</param>
        /// <param name="items">Collection of objects to be cached</param>
        public static void Set(string key, IEnumerable<object> items)
        {
            Cache.Set(key, items);
        }

        /// <summary>
        /// Gets cached collection
        /// </summary>
        /// <param name="key">Key of cached collection</param>
        /// <returns>Collection of objects</returns>
        public static IEnumerable<object> Get(string key)
        {
            
            return Cache.TryGetValue(key, out IEnumerable<object> value) ? value : null;
        }
    }
}
