﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using PrestaCap.DataContracts.ExcelParserConfig;
using PrestaCap.Misc.Extensions;
using PrestaCap.Reporting.Services.Abstraction;

namespace PrestaCap.Reporting.Services.Implementation.Operations
{
    public class DirectMappingOperation : IOperation
    {
        /// <summary>
        /// Gets the values from JToken with no operations
        /// </summary>
        /// <param name="sourceJson">JToken that holds json file information</param>
        /// <param name="header"></param>
        /// <returns>list of values</returns>
        public virtual IEnumerable<object> Operate(JToken sourceJson, Header header)
        {
            var values = sourceJson.SelectTokens(header.JsonPath, true).Convert(Type.GetType($"System.{header.Type}"));
            //Cache the value if required to be used in up coming operations
            if (header.Cached)
                LookupCache.Set(header.JsonPath, values);
            return values;
        }
    }
}
