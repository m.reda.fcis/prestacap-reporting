﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using PrestaCap.DataContracts.ExcelParserConfig;
using PrestaCap.Reporting.Services.Abstraction;

namespace PrestaCap.Reporting.Services.Implementation.Operations
{
    public static class OperationContext
    {
        private static Dictionary<OperationEnum, Lazy<IOperation>> _operations;

        static OperationContext()
        {
            Init();
        }

        /// <summary>
        /// Intialize the operations
        /// </summary>
        private static void Init()
        {
            _operations = new Dictionary<OperationEnum, Lazy<IOperation>>
            {
                {OperationEnum.BooleanBitConversion, new Lazy<IOperation>(() => new BooleanBitConversion())},
                {OperationEnum.DateManipulation, new Lazy<IOperation>(() => new DateManipulation())},
                {OperationEnum.DirectMapping, new Lazy<IOperation>(() => new DirectMappingOperation())}
            };
        }

        /// <summary>
        /// Perform Operation on JToken
        /// </summary>
        /// <param name="sourceJson">JToken that holds json file information</param>
        /// <param name="header"></param>
        /// <returns>list of values</returns>
        public static IEnumerable<object> Operate(JToken sourceJson, Header header)
        {
            return _operations[header.Operation.Name].Value.Operate(sourceJson, header);
        }
    }
}
