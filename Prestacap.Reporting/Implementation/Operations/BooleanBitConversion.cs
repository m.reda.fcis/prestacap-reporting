﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using PrestaCap.DataContracts.ExcelParserConfig;

namespace PrestaCap.Reporting.Services.Implementation.Operations
{
    public class BooleanBitConversion : DirectMappingOperation
    {
        /// <summary>
        /// Gets boolean values from the JSON file and then converts the value to the corsponding byte repsresnation
        /// </summary>
        /// <param name="sourceJson">JToken that holds json file information</param>
        /// <param name="header"></param>
        /// <returns>list of values</returns>
        public override IEnumerable<object> Operate(JToken sourceJson, Header header)
        {
            var value = base.Operate(sourceJson, header);
            return value.Select(Convert.ToByte).Cast<object>();

        }
    }
}
