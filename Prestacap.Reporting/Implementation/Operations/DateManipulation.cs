﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using PrestaCap.DataContracts.ExcelParserConfig;

namespace PrestaCap.Reporting.Services.Implementation.Operations
{
    public class DateManipulation : DirectMappingOperation
    {
        /// <summary>
        /// Get Date values from the JSON file add days to the day
        /// </summary>
        /// <param name="sourceJson">JToken that holds json file information</param>
        /// <param name="header"></param>
        /// <returns>list of values</returns>
        public override IEnumerable<object> Operate(JToken sourceJson, Header header)
        {
            var days =  base.Operate(sourceJson, header).Cast<int>();
            var baseDate = LookupCache.Get(header.Operation.Params.FirstOrDefault()).Cast<DateTime>();

            return  baseDate.Select((date, index) => date.AddDays(days.ElementAt(index))).Cast<object>();
        }
    }
}
