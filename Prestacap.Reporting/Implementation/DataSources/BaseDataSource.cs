﻿using PrestaCap.Reporting.Services.Abstraction;

namespace PrestaCap.Reporting.Services.Implementation.DataSources
{
    public abstract class BaseDataSource:IDataSource
    {
        protected readonly string[] Parameters;

        protected BaseDataSource(params string[] parameters)
        {
            Parameters = parameters;
        }

        public abstract T GetData<T>();
    }
}
