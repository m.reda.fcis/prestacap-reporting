﻿using Amazon;
using PrestaCap.S3.Repository;

namespace PrestaCap.Reporting.Services.Implementation.DataSources
{
    /// <summary>
    ///     Represent a datasource that retrieve data from S3 Bucket
    /// </summary>
    public class S3DataSource : BaseDataSource
    {
        private readonly IS3Repoistory _s3Repoistory;

        /// <param name="parameters">bucket name as the first paramter and filename as second paramter</param>
        public S3DataSource(params string[] parameters) : base(parameters)
        {
#if DEBUG
            _s3Repoistory = new S3Repository("AKIAIE4XYJQS6BZLB3JQ", "xwe3qmu/rjbKMGawkX60dwVwn+kEhsNRDUC4m2SN",
                RegionEndpoint.USEast1);
#else
            _s3Repoistory = new S3Repository();
#endif
        }

        /// <summary>
        ///     Gets Json file from AWS S3
        /// </summary>
        /// <typeparam name="T"> Type that the file will be deserialized to</typeparam>
        /// <returns>Typed object that represents the JSON file</returns>
        public override T GetData<T>()
        {
            return _s3Repoistory.GetObject<T>(Parameters[0], Parameters[1]);
        }
    }
}