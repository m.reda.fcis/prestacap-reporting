﻿using System;
using System.Linq;
using PrestaCap.Misc.Extensions;
using PrestaCap.Misc.Utilities;
using PrestaCap.Reporting.Services.Resources;

namespace PrestaCap.Reporting.Services.Implementation.DataSources
{
    public class FileSystemDataSource : BaseDataSource
    {
        /// <param name="parameters">file name with extension</param>
        public FileSystemDataSource(params string[] parameters) : base(parameters)
        {
        }

        /// <summary>
        ///     Gets Json file from local file system
        /// </summary>
        /// <typeparam name="T"> Type that the file will be deserialized to</typeparam>
        /// <returns>Typed object that represents the JSON file</returns>
        public override T GetData<T>()
        {
            if (!Parameters.Any()) throw new InvalidOperationException(ErrorMessages.MissingConfigFileName);

            return IoUtil.GetDomainTextFile(Parameters[0]).FromStringJson<T>();
        }
    }
}