﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace PrestaCap.Misc.Extensions
{
    public static class ObjectExtensions
    {
        /// <summary>
        /// Converts object to Stream
        /// </summary>
        /// <param name="obj">object to be converted</param>
        /// <returns>Stream of the object</returns>
        public static Stream ToStream(this object obj)
        {
            if (obj == null)
                return null;
            using (var memoryStream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(memoryStream,obj);
                return memoryStream;
            }
            
        }

       
    }
}
