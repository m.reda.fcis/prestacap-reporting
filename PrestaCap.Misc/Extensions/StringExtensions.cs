﻿using Newtonsoft.Json;
using PrestaCap.Misc.Enums;
using PrestaCap.Misc.Utilities;

namespace PrestaCap.Misc.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Deserialize from string json to typed object
        /// </summary>
        /// <typeparam name="T">Type to be deserialized</typeparam>
        /// <param name="str">JSON string</param>
        /// <returns>Typed object that represent the string json</returns>
        public static T FromStringJson<T>(this string str)
        {
            return JsonConvert.DeserializeObject<T>(str,new JsonSerializerSettings(){
 
                DateParseHandling = DateParseHandling.DateTimeOffset
            });

        }
        /// <summary>
        ///  Deserialize object from string json to typed object
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static object FromStringJson(this string str)
        {
            return JsonConvert.DeserializeObject(str);
        }

        /// <summary>
        /// Append extention to file name
        /// </summary>
        /// <param name="fileName">file name without extention</param>
        /// <param name="fileExtension">extension type</param>
        /// <returns>File name with extension appended</returns>
        public static string AppendFileExtension(this string fileName, FileExt fileExtension)
        {
            return $"{fileName}.{EnumUtility.GetDescriptionFromEnum(fileExtension)}";
        }
    }
}
