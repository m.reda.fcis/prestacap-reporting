﻿using System.IO;
using Newtonsoft.Json;

namespace PrestaCap.Misc.Extensions
{
    public static class StreamExtentions
    {
        /// <summary>
        /// Deserialize from Stream JSON to typed object
        /// </summary>
        /// <typeparam name="T">Object type</typeparam>
        /// <param name="stream">Json stream to be deserialized</param>
        /// <returns>Typed object that represent the Stream json</returns>
        public static T FromJsonStream<T>(this Stream stream)
        {
            var serializer = new JsonSerializer()
            {
                DateParseHandling = DateParseHandling.DateTimeOffset
            };
            using (var sr = new StreamReader(stream))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                return serializer.Deserialize<T>(jsonTextReader);
            }
        }
    }
}