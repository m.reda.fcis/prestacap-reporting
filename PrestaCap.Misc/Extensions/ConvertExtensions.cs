﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PrestaCap.Misc.Extensions
{
    public static class ConvertExtensions
    {
        /// <summary>
        /// Convert from object to specefied type
        /// </summary>
        /// <param name="obj">object to be converted</param>
        /// <param name="type">destination type</param>
        /// <returns>object converted to destination type</returns>
        public static object Convert(this object obj, Type type)
        {
            return System.Convert.ChangeType(obj, type);
        }

        /// <summary>
        /// Convert from collection of object to specefied type.
        /// Each element will be converted to the specefied type
        /// </summary>
        /// <param name="objects">collection of objects to be converted</param>
        /// <param name="type">destination type</param>
        /// <returns>object collection converted to destination type</returns>
        public static IEnumerable<object> Convert(this IEnumerable<object> objects, Type type)
        {
            return objects.Select(obj => Convert(obj, type));
            
        }
    }
}
