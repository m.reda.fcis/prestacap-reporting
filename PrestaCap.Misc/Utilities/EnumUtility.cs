﻿using System;
using System.ComponentModel;
using System.Linq;

namespace PrestaCap.Misc.Utilities
{
    public static class EnumUtility
    {
        /// <summary>
        /// Get the value of Description attribute of enum item
        /// </summary>
        /// <param name="enumValue">enum item</param>
        /// <returns>Description the enum item</returns>
        public static string GetDescriptionFromEnum(Enum enumValue)
        {
            var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());
            var description =
                ((DescriptionAttribute)fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false).First())
                .Description;
            return description;
        }

        public static T TryParse<T>(string value) where T:struct
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();

            Enum.TryParse(type, value, true, out var enumValue);

            return (T?) enumValue ?? default(T);
        }
    }
}
