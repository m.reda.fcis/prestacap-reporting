﻿using System;
using System.IO;

namespace PrestaCap.Misc.Utilities
{
    public static class IoUtil
    {
        private static string CurrentDirectory => AppDomain.CurrentDomain.BaseDirectory;

        /// <summary>
        /// Read text file
        /// </summary>
        /// <param name="path">file path</param>
        /// <returns>string contains all the text in the text file</returns>
        public static string GetTextFile(string path)
        {
            return File.ReadAllText(path);
        }


        /// <summary>
        /// Read text file from App domain folder
        /// </summary>
        /// <param name="fileName">file name with extension</param>
        /// <returns>string contains all the text in the text file</returns>
        public static string GetDomainTextFile(string fileName)
        {
            var path = Path.Combine(CurrentDirectory, fileName);
            return GetTextFile(path);
        }
        


    }
}
