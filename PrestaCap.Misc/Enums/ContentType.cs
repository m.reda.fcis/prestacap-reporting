﻿using System.ComponentModel;

namespace PrestaCap.Misc.Enums
{
    /// <summary>
    /// Represents Content-Type MIME 
    /// </summary>
    public enum ContentType
    {
        Unknown,
        [Description("application/json")] Json,
        [Description("plain/text")] Text,
        [Description("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")] Excel
       
    }
}