﻿using System.ComponentModel;

namespace PrestaCap.Misc.Enums
{
    /// <summary>
    /// Represents File Extensions of widely used files
    /// </summary>
    public enum FileExt
    {
        [Description("xlsx")]
        ExcelXlsx,
       
    }
}
