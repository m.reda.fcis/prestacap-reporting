﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PrestaCap.DataContracts.ExcelParserConfig
{
    public class Operation
    {
        /// <summary>
        /// Operation name that will be performed on the values
        /// </summary>
        [JsonProperty("name")]
        [JsonConverter(typeof(StringEnumConverter))]
        public OperationEnum Name { get; set; }

        /// <summary>
        /// Parameters needed for this operation
        /// </summary>
        [JsonProperty("params")]
        public List<string> Params { get; set; }
        
    }

    public enum OperationEnum
    {
        BooleanBitConversion,
        DateManipulation,
        DirectMapping
    }
}