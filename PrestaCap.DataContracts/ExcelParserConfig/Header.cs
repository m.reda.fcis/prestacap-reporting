﻿using Newtonsoft.Json;

namespace PrestaCap.DataContracts.ExcelParserConfig
{
    public class Header
    {
        /// <summary>
        /// Column name
        /// </summary>
        [JsonProperty("name")] public string Name { get; set; }

        /// <summary>
        /// JSON path that this column will be populated with
        /// </summary>
        [JsonProperty("jsonPath")] public string JsonPath { get; set; }

        /// <summary>
        /// Operation that will be performed on the value after being selected
        /// </summary>
        [JsonProperty("operation")] public Operation Operation { get; set; }

        /// <summary>
        /// Type of the values
        /// </summary>
        [JsonProperty("type")] public string Type { get; set; }


        /// <summary>
        /// Cell format of this header
        /// </summary>
        [JsonProperty("formatting")] public string Formatting { get; set; }

        /// <summary>
        /// Cache the values after being selected
        /// </summary>
        [JsonProperty("cached")] public bool Cached { get; set; }
    }
}