﻿using Newtonsoft.Json;

namespace PrestaCap.DataContracts.ExcelParserConfig
{
    public class Properties
    {
        /// <summary>
        /// Column width of Excel workbook
        /// </summary>
        [JsonProperty("columnWidth")] public double ColumnWidth { get; set; }

        /// <summary>
        /// Apply Auto-filter on the columns
        /// </summary>
        [JsonProperty("autoFilter")] public bool AutoFilter { get; set; }

        /// <summary>
        /// Alignment of the workbook cells
        /// </summary>
        [JsonProperty("alignment")] public string Alignment { get; set; }

        /// <summary>
        /// Excel workbook sheet
        /// </summary>
        [JsonProperty("sheetName")] public string SheetName { get; set; }


        /// <summary>
        /// Wrap text in workbook cells
        /// </summary>
        [JsonProperty("wrapText")] public bool WrapText { get; set; }
    }
}