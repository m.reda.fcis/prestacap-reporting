﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace PrestaCap.DataContracts.ExcelParserConfig
{
    public class ParserConfig
    {
        /// <summary>
        /// Excel Headers
        /// </summary>
        [JsonProperty("headers")]
        public List<Header> Headers { get; set; }

        /// <summary>
        /// Excel workbook properties
        /// </summary>
        [JsonProperty("properties")] public Properties Properties { get; set; }


    }
}

