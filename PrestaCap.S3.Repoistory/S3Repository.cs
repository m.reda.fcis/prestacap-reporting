﻿using System;
using System.IO;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using PrestaCap.Misc.Enums;
using PrestaCap.Misc.Extensions;
using PrestaCap.Misc.Utilities;

namespace PrestaCap.S3.Repository
{
    public class S3Repository : IS3Repoistory
    {

        private readonly IAmazonS3 _client;

        #region Public Constructors

        public S3Repository()
        {
            _client = new AmazonS3Client();
         
        }

        public S3Repository(string accessKey, string secretKey, string region)
        {
            _client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.GetBySystemName(region));

        }



        public S3Repository(string accessKey, string secretKey)
        {
            _client = new AmazonS3Client(accessKey, secretKey);
        }

        public S3Repository(string accessKey, string secretKey, RegionEndpoint region)
        {
            _client = new AmazonS3Client(accessKey, secretKey, region);
        }
        

        #endregion

        #region Public Methods

        public Task<GetObjectResponse> GetAsync(string bucketName, string keyName)
        {
            var request = new GetObjectRequest
            {
                BucketName = bucketName,
                Key = keyName
            };
            return _client.GetObjectAsync(request);
        }

        public GetObjectResponse Get(string bucketName, string keyName)
        {
            return GetAsync(bucketName, keyName).Result;
        }

        public T GetObject<T>(string bucketName, string keyName)
        {
            return Get(bucketName, keyName).ResponseStream.FromJsonStream<T>();
        }

        public Task<PutObjectResponse> UploadObject(string bucketName, string key, object objectToUpload, ContentType contentType)
        {
            var putRequestObject = new PutObjectRequest() { BucketName = bucketName, Key = key };

            AttachContent(objectToUpload, putRequestObject);
            AttachContentType(contentType, putRequestObject);

            return _client.PutObjectAsync(putRequestObject);
        }
        

        #endregion

        #region Private Methods

        private static void AttachContentType(ContentType contentType, PutObjectRequest putRequestObject)
        {
            if (contentType != ContentType.Unknown)
                putRequestObject.ContentType = EnumUtility.GetDescriptionFromEnum(contentType);
        }

        private static void AttachContent(object objectToUpload, PutObjectRequest putRequestObject)
        {
            switch (objectToUpload)
            {
                case Stream streamObject:
                    putRequestObject.InputStream = streamObject;
                    break;
                case string stringObject:
                    putRequestObject.ContentBody = stringObject;
                    break;
                default:
                    putRequestObject.InputStream = objectToUpload.ToStream();
                    break;
            }
        }

        #endregion
    }
}