﻿using System.Threading.Tasks;
using Amazon.S3.Model;
using PrestaCap.Misc.Enums;

namespace PrestaCap.S3.Repository
{
    public interface IS3Repoistory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bucketName"></param>
        /// <param name="keyName"></param>
        /// <returns></returns>
        Task<GetObjectResponse> GetAsync(string bucketName, string keyName);

        GetObjectResponse Get(string bucketName, string keyName);

        T GetObject<T>(string bucketName, string keyName);

        Task<PutObjectResponse> UploadObject(string bucketName, string key, object objectToUpload,
            ContentType contentType);
    }
}
