using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using OfficeOpenXml;
using PrestaCap.Reporting.Services.Abstraction;
using PrestaCap.Reporting.Services.Implementation.ExcelProcessor;
using PrestaCap.Reporting.Test.FakeProcessors;
using Xunit;

namespace PrestaCap.Reporting.Test.Tests
{
    public class CellMapperUnitTest : BaseTestClass
    {
        private IExcelProcessor _processor;
        private PostFakeProcessor _fakeProcessor;
       

        [Fact]
        public void ValidCellsValues()
        {
            #region Arrange

            var mockConfigDataSource = MockConfigDataSource();
            var mockInputDataSource = MockInputDataSource("hotelRates.json");
            var mockOutputDestination = MockOutputDestination();

            _processor = new InitFakeProcessor(mockConfigDataSource.Object,
                mockInputDataSource.Object, mockOutputDestination.Object);

            _fakeProcessor = new PostFakeProcessor();
            _processor.SetNextStep(new CellsMapper().SetNextStep(_fakeProcessor));

            var expectedWorksheet = new ExcelPackage(new FileInfo("ExpectedResult.xlsx")).Workbook.Worksheets.First();

            #endregion

            #region Act

            _processor.Execute();

            #endregion

            #region Assert

            Assert.NotNull(_fakeProcessor.ResultWorkSheet);


            for (var i = 1; i < expectedWorksheet.Dimension.Columns; i++)
            for (var j = 1; j < expectedWorksheet.Dimension.Rows; j++)
                Assert.Equal(expectedWorksheet.Cells[j, i].Text, _fakeProcessor.ResultWorkSheet.Cells[j, i].Text);

            #endregion
        }

        [Fact]
        public void InvalidJson_MissingValues()
        {
            #region Arrange

            var mockConfigDataSource = MockConfigDataSource();

            var mockInputDataSource = MockInputDataSource("InvalidHotelRates.json");

            var mockOutputDestination = MockOutputDestination();

            _processor = new InitFakeProcessor(mockConfigDataSource.Object,
                mockInputDataSource.Object, mockOutputDestination.Object);

            _fakeProcessor = new PostFakeProcessor();
            _processor.SetNextStep(new CellsMapper().SetNextStep(_fakeProcessor));

            #endregion

            #region Act & Assert

           Assert.Throws<JsonException>(() => _processor.Execute());

            #endregion
        }
    }
}