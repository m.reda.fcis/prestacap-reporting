using PrestaCap.Reporting.Services.Abstraction;
using PrestaCap.Reporting.Services.Implementation.ExcelProcessor;
using PrestaCap.Reporting.Test.FakeProcessors;
using Xunit;

namespace PrestaCap.Reporting.Test.Tests
{
    public class InitWorkSheetsUnitTest : BaseTestClass
    {
        private readonly IExcelProcessor _processor;
        private readonly PostFakeProcessor _fakeProcessor;

        public InitWorkSheetsUnitTest()
        {
            var mockConfigDataSource = MockConfigDataSource();

            var mockInputDataSource = MockInputDataSource("hotelRates.json");

            var mockOutputDestination = MockOutputDestination();


            _processor = new InitWorkSheets(mockConfigDataSource.Object,
                mockInputDataSource.Object, mockOutputDestination.Object);
            _fakeProcessor = new PostFakeProcessor();
            _processor.SetNextStep(_fakeProcessor);
        }

        [Fact]
        public void FormattingIsCorrect()
        {
            #region Act

            _processor.Execute();

            #endregion

            #region Assert

            Assert.NotNull(_fakeProcessor.ResultWorkSheet);
            Assert.Equal(ParserConfig.Properties.ColumnWidth, _fakeProcessor.ResultWorkSheet.DefaultColWidth);
            Assert.Equal(ParserConfig.Properties.Alignment,
                _fakeProcessor.ResultWorkSheet.Cells.Style.HorizontalAlignment.ToString());
            Assert.Equal(ParserConfig.Properties.WrapText, _fakeProcessor.ResultWorkSheet.Cells.Style.WrapText);

            #endregion
        }
    }
}