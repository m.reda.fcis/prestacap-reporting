﻿using System.IO;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PrestaCap.DataContracts.ExcelParserConfig;
using PrestaCap.Reporting.Services.Abstraction;
using PrestaCap.Reporting.Services.Implementation.ExcelProcessor;
using PrestaCap.Reporting.Test.FakeProcessors;
using Xunit;

namespace PrestaCap.Reporting.Test.Tests
{
    public class ProcessHeadersUnitTest : BaseTestClass
    {
        private readonly IExcelProcessor _processor;
        private readonly PostFakeProcessor _fakeProcessor;
        private readonly ParserConfig _parserConfig;

        public ProcessHeadersUnitTest()
        {
            var mockConfigDataSource = MockConfigDataSource();

            var mockInputDataSource = MockInputDataSource("hotelrates.json");

            var mockOutputDestination = MockOutputDestination();

            ;
            _processor = new InitFakeProcessor(mockConfigDataSource.Object,
                mockInputDataSource.Object, mockOutputDestination.Object);
            _fakeProcessor = new PostFakeProcessor();
            _processor.SetNextStep(new ProcessHeaders().SetNextStep(_fakeProcessor));
        }

        [Fact]
        public void SameHeaderCount()
        {
            #region Act

            _processor.Execute();

            #endregion

            #region Assert

            Assert.NotNull(_fakeProcessor.ResultWorkSheet);
            Assert.Equal(ParserConfig.Headers.Count, _fakeProcessor.ResultWorkSheet.Dimension.Columns);

            #endregion
        }
    }
}
