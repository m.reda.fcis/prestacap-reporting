﻿using System.IO;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PrestaCap.DataContracts.ExcelParserConfig;
using PrestaCap.Reporting.Services.Abstraction;

namespace PrestaCap.Reporting.Test.Tests
{
    public class BaseTestClass
    {
        protected static ParserConfig ParserConfig =>
            JsonConvert.DeserializeObject<ParserConfig>(File.ReadAllText("Config.json"));

        protected static Mock<IDataSource> MockConfigDataSource()
        {
            var configDataSource = new Mock<IDataSource>();
            configDataSource.Setup(c => c.GetData<ParserConfig>())
                .Returns(ParserConfig);

            return configDataSource;
        }

        protected static Mock<IDataSource> MockInputDataSource(string fileName)
        {
            var inputDataSource = new Mock<IDataSource>();
            inputDataSource.Setup(c => c.GetData<JToken>())
                .Returns(JsonConvert.DeserializeObject<JToken>(File.ReadAllText(fileName), new JsonSerializerSettings { DateParseHandling = DateParseHandling.DateTimeOffset }));

            return inputDataSource;
        }

        protected static Mock<IOutputDestination> MockOutputDestination()
        {
            var mockOutputDestination = new Mock<IOutputDestination>();
            mockOutputDestination.Setup(c => c.Save(It.IsAny<MemoryStream>()));
            return mockOutputDestination;
        }

    }
}
