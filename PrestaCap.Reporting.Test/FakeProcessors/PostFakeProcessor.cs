using OfficeOpenXml;
using PrestaCap.Reporting.Services.Implementation.ExcelProcessor;

namespace PrestaCap.Reporting.Test.FakeProcessors
{
    internal class PostFakeProcessor : BaseExcelProcessor
    {
        public ExcelWorksheet ResultWorkSheet ;

        public override void Execute()
        {
            ResultWorkSheet = MainWorkSheet;

        }
    }
}