using OfficeOpenXml;
using OfficeOpenXml.Style;
using PrestaCap.Misc.Utilities;
using PrestaCap.Reporting.Services.Abstraction;
using PrestaCap.Reporting.Services.Implementation.ExcelProcessor;

namespace PrestaCap.Reporting.Test.FakeProcessors
{
    internal class InitFakeProcessor : BaseExcelProcessor
    {
      

        public InitFakeProcessor(IDataSource configSource, IDataSource fileSource, IOutputDestination outputDestination) :
            base(configSource, fileSource, outputDestination)
        {
            
        }
        public override void Execute()
        {
          
            if(Package.Workbook.Worksheets[ParserConfig.Properties.SheetName] == null)
                Package.Workbook.Worksheets.Add(ParserConfig.Properties.SheetName);


            MainWorkSheet.DefaultColWidth = ParserConfig.Properties.ColumnWidth;
            MainWorkSheet.Cells.Style.HorizontalAlignment =
                EnumUtility.TryParse<ExcelHorizontalAlignment>(ParserConfig.Properties.Alignment);
            MainWorkSheet.Cells.Style.WrapText = ParserConfig.Properties.WrapText;
            NextStep.Execute();

           

        }
    }

}
